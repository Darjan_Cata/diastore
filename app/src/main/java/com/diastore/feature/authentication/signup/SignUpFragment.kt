package com.diastore.feature.authentication.signup

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.diastore.R
import com.diastore.SignUpBinding
import com.diastore.util.BaseFragment
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignUpFragment : BaseFragment<SignUpBinding, SignUpViewModel>(R.layout.fragment_signup) {
    override val viewModel by sharedViewModel<SignUpViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        binding.textLogin.setOnClickListener {
            findNavController().navigate(SignUpFragmentDirections.actionSignUpFragmentToLogInFragment())
        }

        binding.buttonSignUp.setOnClickListener{
            findNavController().navigate(SignUpFragmentDirections.actionSignUpFragmentToAboutYouFragment())
        }

    }
}