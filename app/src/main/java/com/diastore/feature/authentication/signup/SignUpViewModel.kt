package com.diastore.feature.authentication.signup

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.diastore.model.User
import com.diastore.service.UserService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class SignUpViewModel(private val userService: UserService) : ViewModel() {

    val firstName = MutableLiveData<String>("")
    val lastName = MutableLiveData<String>("")
    val email = MutableLiveData<String>("")
    val password = MutableLiveData<String>("")
    val age = MutableLiveData<String>("")
    val weight = MutableLiveData<String>("")
    val height = MutableLiveData<String>("")
    val carbsToInsulin = MutableLiveData<String>("")
    val bloodSugarToInsulin = MutableLiveData<String>("")

    private val _signUpResponse = MutableLiveData<User>()
    val signUpResponse: LiveData<User>
        get() = _signUpResponse

    private val _isLoading = MutableLiveData<Boolean>(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    fun signUp() {
        CoroutineScope(Dispatchers.IO).launch {
            val request = userService.registerOrUpdateUser(getUser())

            withContext(Dispatchers.Main) {
                _isLoading.value = true
                try {
                    _signUpResponse.value = getUser()
                    request.await()
                    Log.d("WRKRSignUp", _signUpResponse.value.toString())
                    _isLoading.value = false
                } catch (e: Throwable) {
                    Log.e("WRKR", e.toString())
                    _isLoading.value = false
//                    _getError.value = e.message
                }
            }
        }
    }

    fun getUser(): User =
        User(
            UUID.randomUUID().toString(),
            firstName.value!!,
            lastName.value!!,
            email.value!!,
            password.value!!,
            weight.value!!.toInt(),
            height.value!!.toInt(),
            carbsToInsulin.value!!.toDouble(),
            bloodSugarToInsulin.value!!.toDouble(),
            "0001-01-01T00:00:00"
        )
}