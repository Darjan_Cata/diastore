package com.diastore.feature.home

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.UUID

@Parcelize
data class Entry(
    val id: UUID = UUID.randomUUID(),
    val bloodSugarLevel: Int,
    val carbohydratesIntake: Int,
    val insulinIntake: Float,
    val entryTime: String,
    val entryHour: String,
    val physicalActivityDuration: Int = 0,
    val entryMomentSpecifier: MomentSpecifier? = null,
    val mealTypeSpecifier: MealTypeSpecifier? = null,
    val hasDonePhysicalActivity: Boolean = false
): Parcelable

enum class MomentSpecifier {
    BEFORE_MEAL, AFTER_MEAL
}

enum class MealTypeSpecifier {
    BREAKFAST, LUNCH, DINNER, SNACK
}