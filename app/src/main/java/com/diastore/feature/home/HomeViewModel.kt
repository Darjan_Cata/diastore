package com.diastore.feature.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel(){
    private val _entries = MutableLiveData<List<Entry>>()
    val entries: LiveData<List<Entry>>
        get() = _entries

    init {
        val initEntries = mutableListOf<Entry>()
        repeat((1..100).count()) {
            initEntries.add(
                Entry(
                    bloodSugarLevel = 100 + it,
                    carbohydratesIntake = 1 + it,
                    insulinIntake = 1F + it,
                    entryTime = "10/10/10",
                    entryHour = "17:35",
                    physicalActivityDuration = 45 + it,
                    mealTypeSpecifier = MealTypeSpecifier.LUNCH,
                    entryMomentSpecifier = MomentSpecifier.BEFORE_MEAL
                )
            )
        }
        _entries.value = initEntries
    }

    fun deleteEntry(entry: Entry) {
        val tempEntries = _entries.value?.toMutableList() ?: mutableListOf()
        tempEntries.removeIf {
            it.id == entry.id
        }
        _entries.value = tempEntries
    }

    fun handleSelectedEntryChange(selectedEntry: Entry) {
        val tempEntries = _entries.value?.toMutableList() ?: mutableListOf()
        val containsEntry = tempEntries.find { it.id == selectedEntry.id }
        if (containsEntry != null) {
            tempEntries.forEachIndexed { index, entry ->
                if (entry.id == selectedEntry.id) {
                    tempEntries[index] = selectedEntry
                    return@forEachIndexed
                }
            }
        } else {
            tempEntries.add(0, selectedEntry)
        }
        _entries.value = tempEntries
    }
}