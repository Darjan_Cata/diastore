package com.diastore.feature.home

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.diastore.DiaStoreActivity
import com.diastore.DrawerHeaderBinding
import com.diastore.HomeBinding
import com.diastore.R
import com.diastore.util.BaseFragment
import com.diastore.util.SharedPreferencesManager
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<HomeBinding, HomeViewModel>(R.layout.fragment_home) {
    override val viewModel by viewModel<HomeViewModel>()
    private val adapter: EntryAdapter = EntryAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        val user = SharedPreferencesManager(requireActivity() as DiaStoreActivity).getCurrentUser()
        Log.d("WRKRSharedAtHome", user.toString())

        binding.navView.setupWithNavController(findNavController())
        val headerBinding = DrawerHeaderBinding.bind(binding.navView.getHeaderView(0))
        headerBinding.userName = user.firstName + " " + user.lastName
        
        val list = mutableListOf<Entry>()
        for(i in (0..9)) {
            list.add(
                Entry(
                    bloodSugarLevel = 120 + i,
                    carbohydratesIntake = 10,
                    insulinIntake = 1f,
                    entryTime = "10/10/10",
                    entryHour = "13:37",
                    entryMomentSpecifier = MomentSpecifier.BEFORE_MEAL,
                    mealTypeSpecifier = MealTypeSpecifier.LUNCH
                )
            )
        }
        adapter.setEntriesList(list)

        binding.entriesRecycler.adapter = adapter.apply {
            setOnEntryClickListener {
                Toast.makeText(context, "Details", Toast.LENGTH_SHORT).show()
                /*findNavController().navigate(
                    HomeFragmentDirections.actionHomeFragmentToEntryDetailsFragment(
                        it
                    )
                )*/
            }
            setOnDeleteEntryClickListener {
                viewModel.deleteEntry(it)
            }
        }

        binding.navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.challenges -> findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToChallengesFragment())
                R.id.settings -> findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToSettingsFragment())
                R.id.about -> findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToAboutFragment())
                R.id.statistics -> Toast.makeText(context, it.title, Toast.LENGTH_SHORT).show()
            }
            true
        }

        binding.toolbar.setNavigationOnClickListener {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
    }
}