package com.diastore.feature.challenges

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.diastore.ChallengesBinding
import com.diastore.R
import com.diastore.util.DataBindingFragment

class ChallengesFragment : DataBindingFragment<ChallengesBinding>(R.layout.fragment_challenges) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }
}