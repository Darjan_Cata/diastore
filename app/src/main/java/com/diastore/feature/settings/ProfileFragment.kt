package com.diastore.feature.settings

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.diastore.DiaStoreActivity
import com.diastore.ProfileBinding
import com.diastore.R
import com.diastore.util.BaseFragment
import com.diastore.util.SharedPreferencesManager
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.system.exitProcess

class ProfileFragment : BaseFragment<ProfileBinding, SettingsViewModel>(R.layout.fragment_profile) {
    override val viewModel by viewModel<SettingsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        val sharedPreferencesManager = SharedPreferencesManager(activity as DiaStoreActivity)
        viewModel.setUser(sharedPreferencesManager.getCurrentUser())

        binding.buttonLogout.setOnClickListener {
            sharedPreferencesManager.clearUserData()
            AlertDialog.Builder(context).setTitle(R.string.settings_log_out_string)
                .setMessage("Are you sure?")
                .setPositiveButton("YES") { _, _ ->
                    (activity as DiaStoreActivity).finishAffinity()
                    exitProcess(0)
                }
                .setNegativeButton("NO") { dialog, _ ->
                    dialog.dismiss()
                }
                .show()

        }
    }
}