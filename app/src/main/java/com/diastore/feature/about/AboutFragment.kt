package com.diastore.feature.about

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.diastore.AboutBinding
import com.diastore.R
import com.diastore.util.DataBindingFragment

class AboutFragment : DataBindingFragment<AboutBinding>(R.layout.fragment_about) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        binding.toc.setOnClickListener {
            Toast.makeText(context, "TnC", Toast.LENGTH_SHORT).show()
        }

        binding.privpol.setOnClickListener {
            Toast.makeText(context, "PnP", Toast.LENGTH_SHORT).show()
        }

        binding.buttonFacebook.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/thediabeticjourneyorg/")))
        }

        binding.buttonInstagram.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://instagram.com/thediabeticjourney?igshid=n7pm2ok2vmpu")))
        }

        binding.buttonTwitter.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/thediabjourney?s=09")))
        }
    }
}